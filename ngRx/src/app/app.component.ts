import { Component } from '@angular/core';
import {Store} from "@ngrx/store";
import {clear, countSelector, decrease, increase, updatedAtSelector} from "./reducers/counter";
import {map} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  updatedAt$ = this.store.select(updatedAtSelector);

  count$ = this.store.select(countSelector);
  cannotDecrement$ = this.count$
    .pipe(map(count => count <= 0))


  constructor( private store: Store) {
  }

  increment() {
    this.store.dispatch(increase());
  }

  decrement() {
    this.store.dispatch(decrease());
  }

  clear() {
    this.store.dispatch(clear());
  }
}
